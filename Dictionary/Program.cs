﻿using System;
using System.Linq;

namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            var arr = Console.ReadLine().Split(' ').Select(s => int.Parse(s)).ToArray();
            int numTaxi = 0, cnt = 0;
            var fours = arr.Count(x => x == 4);
            var three = arr.Count(x => x == 3);
            var two = arr.Count(x => x == 2);
            var one = arr.Count(x => x == 1);
            numTaxi += fours;
            if (three <= one)
            {
                numTaxi += three;
                one -= three;
                three = 0;
            }
            else if (three >= one)
            {
                numTaxi += one;
                three -= one;
                one = 0;
            }
            if (two % 2 == 0)
            {
                numTaxi += two / 2;
                two = 0;

            }
            else if (two % 2 != 0)
            {
                numTaxi += two / 2;
                two = 1;

            }
            numTaxi += three;
            if (one > 4)
            {
                numTaxi += one / 4;
                one %= 4; 
            }
            if (two != 0)
            {
                if (one >= 2)
                {
                    one -= 2;

                }
                else if (one != 0)
                    one = 0;
                numTaxi++;
                two--;
            }
            if (one != 0)
            {
                numTaxi++;
            }


            Console.WriteLine(numTaxi);
            
        }
    }
}
