﻿using System;

namespace Struct
{
    struct MyStruct
    {
        public class Nested
        {
            public void Show()
            {
                Console.WriteLine("I am in nested");
            }
        }

       
    }
    class Program
    {
        static void Main(string[] args)
        {
            MyStruct.Nested instnce = new MyStruct.Nested();
            instnce.Show();

            Console.ReadKey();
        }
    }

}
