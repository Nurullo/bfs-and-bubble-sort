﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThreadSafe
{
    class MyClass
    {
        private int classVariable;
        private Object myLock = new Object();
        public void Variable(object parameterVariable)
        {
            lock (myLock)
            {
                this.classVariable = Convert.ToInt32(parameterVariable);

                for (int i = 0; i < 4; i++)
                {
                    Console.WriteLine(this.classVariable + " " + parameterVariable);
                }
            }
        }
    }
}
