﻿using System;
using System.Threading;

namespace ThreadSafe
{
    class Program
    {
        static Maths mathInstance = new Maths();
        static void Main(string[] args)
        {
            #region Thread Safe 1
            //MyClass instance = new MyClass();

            //Thread th1 = new Thread(instance.Variable);
            //th1.Start(1);

            //instance.Variable(3);
            //instance.Variable(4);

            #endregion

            #region Thread Safe 2
            Thread t1 = new Thread(mathInstance.Divide);
            t1.Start();
            mathInstance.Divide();

            #endregion

            Console.ReadKey();
        }
    }
}
