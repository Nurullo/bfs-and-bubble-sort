﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThreadSafe
{
    class Maths
    {
        public int Num1;
        public int Num2;

        Random rand = new Random();

        public void Divide()
        {
            for (int i = 0; i < 100000; i++)
            {
                lock (this)
                {
                    Num1 = rand.Next(1, 2);
                    Num2 = rand.Next(1, 2);
                    int result = Num1 / Num2;
                    Num1 = 0;
                    Num2 = 0;
                }
                
            }
        }
    }
}
