﻿using System;
using System.Diagnostics;
using System.Threading;

namespace BubbleSort
{
    class Program
    {
        static int[] arr = new int[24] { 0, 1, 2, 4, 4, 5, 5, 6, 6, 7, 8, 11, 11, 13, 15, 16, 55, 55, 65, 65, 232, 343, 545, 23245 };
        static bool flag = false;
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            for (int r = 0; r < arr.Length-1; r++)
            {
                flag = false;
                sw.Start();
                for (int i = 0; i < arr.Length-1; i++)
                {
                    if (arr[i] > arr[i + 1])
                    {
                        Swap(i, i + 1);
                    }
                }
                if (flag == false)
                {
                    break;
                }
                
            }
            sw.Stop();
            var total = sw.Elapsed;
            Console.WriteLine(total);
            Console.WriteLine(string.Join(",",arr));

            Console.ReadKey();
        }

        private static void Swap(int v1, int v2)
        {
            int hand = arr[v1];
            arr[v1] = arr[v2];
            arr[v2] = hand;
            flag = true;
        }
    }
}
